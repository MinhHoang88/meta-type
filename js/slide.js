$(document).ready(function () {
  window.addEventListener("click", function (e) {
    if (document.getElementById("logo-menu-header").contains(e.target)) {
      $(".menu-mobile").slideDown();
      $(".btn-menu-close").slideDown();
      $(".logo-menu-header").slideUp();
    } else {
      $(".menu-mobile").slideUp();
      $(".btn-menu-close").slideUp();
      $(".logo-menu-header").slideDown();
    }
  });
  $(".image-slider").slick({
    // centerMode: true,
    // centerPadding: "60px",
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    dots: true,
    centerMode: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          // centerMode: true,
        },
      },
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          dots: true,
          infinite: true,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: true,
          infinite: true,
          autoplay: true,
          autoplaySpeed: 2000,
        },
      },
    ],
  });
});
$(document).on("click", "#logo-menu-header", function (event) {
  event.preventDefault();
});
