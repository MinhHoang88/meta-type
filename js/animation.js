window.scrollTo({
  top: 0,
  left: 0,
  behavior: "smooth",
});
$("html,body").animate({
  scrollTop: 0,
});

// ==== click IOS
$(document).on("click", "#showTextFive-light", function (event) {
  event.preventDefault();
});

///////////////////
function reveal() {
  var reveals_top = document.querySelectorAll(".animation_top");
  var reveals_left = document.querySelectorAll(".animation_left");
  var reveals_bottom = document.querySelectorAll(".animation_bottom");
  var reveals_right = document.querySelectorAll(".animation_right");

  for (var i = 0; i < reveals_top.length; i++) {
    var windowHeight = window.innerHeight;
    var elementTop = reveals_top[i].getBoundingClientRect().top;
    var elementVisible = 150;

    if (elementTop < windowHeight - elementVisible) {
      reveals_top[i].classList.add("active");
    } else {
      reveals_top[i].classList.remove("active");
    }
  }
  for (var i = 0; i < reveals_left.length; i++) {
    var windowHeight = window.innerHeight;
    var elementTop = reveals_left[i].getBoundingClientRect().top;
    var elementVisible = 150;

    if (elementTop < windowHeight - elementVisible) {
      reveals_left[i].classList.add("active");
    } else {
      reveals_left[i].classList.remove("active");
    }
  }
  for (var i = 0; i < reveals_bottom.length; i++) {
    var windowHeight = window.innerHeight;
    var elementTop = reveals_bottom[i].getBoundingClientRect().top;
    var elementVisible = 150;

    if (elementTop < windowHeight - elementVisible) {
      reveals_bottom[i].classList.add("active");
    } else {
      reveals_bottom[i].classList.remove("active");
    }
  }
  for (var i = 0; i < reveals_right.length; i++) {
    var windowHeight = window.innerHeight;
    var elementTop = reveals_right[i].getBoundingClientRect().top;
    var elementVisible = 150;

    if (elementTop < windowHeight - elementVisible) {
      reveals_right[i].classList.add("active");
    } else {
      reveals_right[i].classList.remove("active");
    }
  }
}

window.addEventListener("scroll", reveal);
